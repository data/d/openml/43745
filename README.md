# OpenML dataset: Delinquency-Telecom-Dataset

https://www.openml.org/d/43745

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Delinquency is a condition that arises when an activity or situation does not occur at its scheduled (or expected) date i.e., it occurs later than expected.
Content
Many donors, experts, and microfinance institutions (MFI) have become convinced that using mobile financial services (MFS) is more convenient and efficient, and less costly, than the traditional high-touch model for delivering microfinance services. MFS becomes especially useful when targeting the unbanked poor living in remote areas. The implementation of MFS, though, has been uneven with both significant challenges and successes.
Today, microfinance is widely accepted as a poverty-reduction tool, representing 70 billion in outstanding loans and a global outreach of 200 million clients.
Data Description
https://www.googleapis.com/download/storage/v1/b/kaggle-user-content/o/inbox2F43788582F8d6c62159a033854dc4ca79d2cfbf0942FCapture.PNG?generation=1589482946434860alt=media
A Telecom collaborates with an MFI to provide micro-credit on mobile balances to be paid back in 5 days. The Consumer is believed to be delinquent if he deviates from the path of paying back the loaned amount within 5 days.
The sample data from our client database is hereby given to you for the exercise.
Exercise
Create a delinquency model which can predict in terms of a probability for each loan transaction, whether the customer will be paying back the loaned amount within 5 days of insurance of loan (Label 1  0)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43745) of an [OpenML dataset](https://www.openml.org/d/43745). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43745/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43745/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43745/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

